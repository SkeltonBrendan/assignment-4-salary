// Assignment 4 - Salary
// Brendan Skelton

#include <iostream>
#include <conio.h>
#include <string>

using namespace std;

struct Employee 
{
	int id;
	string first;
	string last;
	float pay;
	int hours;
};


int main()
{
	Employee employee[5];
	for (int i = 1; i <= 5; i++) 
	{
		cout << "Please enter employee #" << i << "'s ID number: ";
		cin >> employee[i].id;
		cout << "\n";
		cout << "Please enter employee #" << i << "'s first name: ";
		cin >> employee[i].first;
		cout << "\n";
		cout << "Please enter employee #" << i << "'s last name: ";
		cin >> employee[i].last;
		cout << "\n";
		cout << "Please enter employee #" << i << "'s pay: ";
		cin >> employee[i].pay;
		cout << "\n";
		cout << "Please enter employee #" << i << "'s amount of work hours: ";
		cin >> employee[i].hours;
		cout << "\n";
	}
	
	float sum[5];
	float total = 0;

	for (int i = 1; i <= 5; i++)
	{
		cout << "Employee ID: " << employee[i].id << "\n";
		cout << "Employee's First Name: " << employee[i].first << "\n";
		cout << "Employee's Last Name: " << employee[i].last << "\n";
		sum[i] = employee[i].pay * employee[i].hours;
		cout << "Employee's Gross Pay: " << sum[i] << "\n" << "\n";
		total += sum[i];
	} 
	cout << "The total gross pay for all employees is: " << total;
	_getch();
	return 0;
}
